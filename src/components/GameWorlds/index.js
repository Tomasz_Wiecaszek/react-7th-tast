
import Header from "../../components/Header";
import './style.css';
import {useRef } from "react";

const GameWorlds = () => {

  const refPol=useRef(null);
  const refLas=useRef(null);
  const refDżu=useRef(null);
  const refPus=useRef(null);
  const refGór=useRef(null);
  return (
    <div>
      <div className="main-container">
        <Header{...{refPol,refLas,refDżu,refPus,refGór}}/>
        <div className="col-12 destination" ref={refPol}><p>Polana</p></div>
        <div className="col-12 destination" ref={refLas}><p>Las</p></div>
        <div className="col-12 destination" ref={refDżu}><p>Dżungla</p></div>
        <div className="col-12 destination" ref={refPus}><p>Pustynia</p></div>
        <div className="col-12 destination" ref={refGór}><p>Góry</p></div>

     </div>
    </div>

  
  );
};

export default GameWorlds;
