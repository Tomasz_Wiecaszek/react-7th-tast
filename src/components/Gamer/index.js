import './style.css'
import React from 'react'
import OponentStats from './OponentStats'
import StatisticTable from './StatisticTable'

const Gamer =({alive})=>{
    
    return(
<div className="col-6">
    <OponentStats alive={alive} render={myStats=>{return(<StatisticTable {...myStats}/>)}}/>
    </div>
    )}

export default Gamer;